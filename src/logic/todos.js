const ns = 'qgo/assessment'
export const ADD_ITEM = `${ns}/ADD_ITEM`;
export const UPDATE_ITEM_STATUS = `${ns}/UPDATE_ITEM_STATUS`;
export const DELETE_ITEM = `${ns}/DELETE_ITEM`;
export const TOGGLE_FILTER = `${ns}/TOGGLE_FILTER`;

export const sortItemsById = (items) => items.sort((a, b) => a.id - b.id);

export const addItem = content => ({
  type: ADD_ITEM, content
});

export const updateItemStatus = payload => ({
  type: UPDATE_ITEM_STATUS, payload
});

export const deleteItem = payload => ({
  type: DELETE_ITEM, payload
});

export const toggleFilter = () => ({
  type: TOGGLE_FILTER
});

export const initialState = {
  items: [
    { id: 1, content: 'Call mum', completed: false },
    { id: 2, content: 'Buy cat food', completed: false },
    { id: 3, content: 'Water the plants', completed: false },
  ],
  filterCompletedItems: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_ITEM:
      const nextId =
        state.items.reduce((id, item) => Math.max(item.id, id), 0) + 1;
      const newItem = {
        id: nextId,
        content: action.content,
        completed: false
      };

      return {
        ...state,
        items: [...state.items, newItem],
      };

    case UPDATE_ITEM_STATUS:
      const {payload} = action;
      const updatedItem = { ...payload, completed: !payload.completed };
      const returnItems = [...state.items.filter(item => item.id !== payload.id), updatedItem];
      return { ...state, items: sortItemsById(returnItems) };

    case DELETE_ITEM:
      return { ...state, items: state.items.filter(item => item.id !== action.payload) };

    case TOGGLE_FILTER:
      return { ...state, filterCompletedItems: !state.filterCompletedItems };

    default:
      return state;
  }
};

export default reducer;
