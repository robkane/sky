import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { toggleFilter } from '../../logic/todos';

export const FilterButton = ({shouldFilter, onFilterToggle}) => (
  <button className="filter-button" onClick={() => onFilterToggle()}>
    {shouldFilter ? 'Show Completed' : 'Hide Completed'}
  </button>
);

FilterButton.PropTypes = {
  onFilterToggle: PropTypes.func
};

const mapDispatchToProps = dispatch => ({
  onFilterToggle: itemId => dispatch(toggleFilter()),
});

const mapStateToProps = state => ({
  shouldFilter: state.todos.filterCompletedItems
});

export default connect(mapStateToProps, mapDispatchToProps)(FilterButton);
