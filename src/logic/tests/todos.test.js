import reducer, {
  initialState,
  addItem,
  deleteItem,
  updateItemStatus,
  toggleFilter
} from '../todos';

const mockState = {
      items: [
        { id: 1, content: 'first', completed: false },
        { id: 2, content: 'second', completed: false },
      ],
      filterCompletedItems: false
    }

describe('reducer', () => {
  it('should return state for unknown action', () => {
    const mockState = { test: 'testItem' };
    const mockAction = { type: 'mystery-meat' };
    const result = reducer(mockState, mockAction);
    expect(result).toEqual(mockState);
  });

  it('should use initial state if state not provided', () => {
    const mockAction = { type: 'mystery-meat' };
    const result = reducer(undefined, mockAction);
    expect(result).toEqual(initialState);
  });

  it('should add new items on ADD_ITEM', () => {
    const mockAction = addItem('third');
    const result = reducer(mockState, mockAction);
    expect(result.items).toHaveLength(3);
    expect(result.items[2].id).toEqual(3);
    expect(result.items[2].content).toEqual('third');
  });

  it('should remove an item on DELETE_ITEM', () => {
    const mockAction = deleteItem(2)
    const result = reducer(mockState, mockAction);
    expect(result.items).toHaveLength(1);
  });

  it('should update an item on UPDATE_ITEM_STATUS', () => {
    const mockAction = updateItemStatus(mockState.items[0]);
    const result = reducer(mockState, mockAction);
    expect(result.items[0].completed).toEqual(true);
  });

  it('should update filter items state on TOGGLE_FILTER', () => {
    mockState.items[0].completed = true;
    const mockAction = toggleFilter();
    const result = reducer(mockState, mockAction);
    expect(result.filterCompletedItems).toEqual(true)
  });

});
