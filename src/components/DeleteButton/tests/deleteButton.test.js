import React from 'react';
import { shallow } from 'enzyme';
import { DeleteButton } from '../';
import renderer from 'react-test-renderer';

const defaultProps = {
  onDelete: f => f,
  id: 1
};

describe('DeleteButton', () => {

  it('renders correctly', () => {
    const tree = renderer
      .create(<DeleteButton {...defaultProps} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

});


