import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { deleteItem } from '../../logic/todos';
import './styles.css';

export const DeleteButton = ({id, onDelete}) => (
    <button
      className="delete-button" 
      onClick={() => onDelete(id)}
      key={id}
    >
    X
    </button>
);

DeleteButton.PropTypes = {
  id: PropTypes.number,
  onDelete: PropTypes.func
};

const mapDispatchToProps = dispatch => ({
  onDelete: itemId => dispatch(deleteItem(itemId)),
});

export default connect(null, mapDispatchToProps)(DeleteButton);
