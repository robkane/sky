import React from 'react';
import { shallow, mount } from 'enzyme';
import { ToDoItem } from '../';

const defaultProps = {
  onUpdate: f => f,
  item: {
    id: 1,
    content: 'Test Content',
    completed: false
  }
};

const completedProps = {
  onUpdate: f => f,
  item: {
    id: 2,
    content: 'Test Content',
    completed: true
  }
};

describe('ToDoItem', () => {
  it('renders without crashing', () => {
    shallow(<ToDoItem {...defaultProps} />);
  });

  it('Should render with the the correct [incomplete] classname', () => {
    const incomplete = shallow(<ToDoItem {...defaultProps} />);
    expect(incomplete.find('.item-incomplete')).toHaveLength(1);
  });

  it('Should render with the the correct [complete] classname', () => {
    const complete = shallow(<ToDoItem {...completedProps} />);
    expect(complete.find('.item-completed')).toHaveLength(1);
  });

});