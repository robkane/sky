import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ToDoItem from '../ToDoItem'
import './styles.css';

export const ItemsList = ({shouldFilter, items}) => {
  const displayItems = shouldFilter ? 
    items.filter(item => item.completed !== true) :
    items;

  return (
    <div>
      <ul className="itemsList-ul">
        {items.length < 1 && <p id="items-missing">Add some tasks above.</p>}
        {displayItems.map(item => (
          <ToDoItem 
            className="todo-item" 
            key={item.id}
            item={item}
          />
        ))}
      </ul>
    </div>
  );
};

ItemsList.propTypes = {
  shouldFilter: PropTypes.bool,
  items: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  items: state.todos.items,
  shouldFilter: state.todos.filterCompletedItems
});

export default connect(mapStateToProps)(ItemsList);
