import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { updateItemStatus } from '../../logic/todos';
import DeleteButton from '../DeleteButton'
import './styles.css';

export const ToDoItem = ({item, onUpdate}) => (
  <li 
    key={item.id}
    >
    <span 
      onClick={() => onUpdate(item)} 
      className={item.completed ? "item-completed" : 'item-incomplete'}
    >
      {item.content}
    </span>
    &nbsp;
    <span>
      <DeleteButton id={item.id} />
    </span>
  </li>
);

ToDoItem.PropTypes = {
  item: PropTypes.shape({
    id: PropTypes.number.isRequired,
    content: PropTypes.string.isRequired,
    completed: PropTypes.bool
  }).isRequired,
  onUpdate: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => ({
  onUpdate: itemToUpdate => dispatch(updateItemStatus(itemToUpdate))
});

export default connect(null, mapDispatchToProps)(ToDoItem);



