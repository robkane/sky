import React from 'react';
import { FilterButton } from '../';
import renderer from 'react-test-renderer';

const defaultProps = {
  onFilter: f => f,
  shouldFilter: false
};

describe('FilterButton', () => {

  it('renders correctly', () => {
    const tree = renderer
      .create(<FilterButton {...defaultProps} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

});


